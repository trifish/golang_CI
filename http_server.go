package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func httpServer(w http.ResponseWriter, r *http.Request) {
	//解析参数
	err := r.ParseForm()
	if err != nil {
		Error(err, "ParseForm Failed:")
	}

	requestBody, err1 := ioutil.ReadAll(r.Body)

	if err1 != nil {
		Error(err1, "RequestBody not Exist:")
	}

	fmt.Println("RequestHeader:", r.Header)
	fmt.Println("RequestBody:", string(requestBody[:]))

	mapUnmars(requestBody)

	defer r.Body.Close()
}
