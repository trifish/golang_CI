package main

import (
	"encoding/json"
	"fmt"
)

func mapUnmars(postR []byte) {
	//解析JSON
	var pR map[string]interface{}
	err := json.Unmarshal(postR, &pR)
	if err != nil {
		Error(err, "JASON decode failed:")
	}

	fmt.Println("Object_kind:", pR["object_kind"])
	fmt.Printf("\n")
}
