package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("Start Server in Master without changing the .yml file")
	http.HandleFunc("/", httpServer)

	err := http.ListenAndServe(":9090", nil)

	if err != nil {
		Error(err, "Http ListenAndServe Wrong:")
		return
	}
}
