package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os/exec"
)

func Error(err error, errTag string) {
	log.Fatalln("Error:", errTag, err)
	fmt.Println("Error:", errTag, err)
}

func execCommand_test(commandName string, params []string) bool {
	cmd := exec.Command(commandName, params...)

	//显示运行的命令
	fmt.Println(cmd.Args)

	stdout, err := cmd.StdoutPipe()

	//fmt.Println(stdout)

	if err != nil {
		fmt.Println(err)
		return false
	}

	cmd.Start()

	reader := bufio.NewReader(stdout)

	//fmt.Println(reader)

	//实时循环读取输出流中的一行内容
	for {
		line, err2 := reader.ReadString('\n')

		//fmt.Println(line)

		if err2 != nil || io.EOF == err2 {
			break
		}
		fmt.Println(line)
	}

	cmd.Wait()
	return true
}

//func main() {
//	command := "git"
//	params1 := []string{"add", "."}
//	params2 := []string{"commit", "-m", "\"Modifid by Golang and Run more than one command\""}
//	params3 := []string{"push", "-u", "origin", "master"}
//	params4 := []string{"pull"}
//	execCommand_test(command, params1)
//	execCommand_test(command, params2)
//	execCommand_test(command, params3)
//	execCommand_test(command, params4)
//	//execCommand_gitcmd()
//}

//func execCommand_gitcmd() {
//	cmd := exec.Command("C:\\Program Files\\Git\\git-cmd.exe", "dir")
//	in := bytes.NewBuffer(nil)
//	cmd.Stdin = in //绑定输入
//	var out bytes.Buffer
//	cmd.Stdout = &out //绑定输出
//
//in.WriteString("go version\n")
//in.WriteString("go env\n")
//		in.WriteString("git add .")
//		in.WriteString("git commit -m \"Commit by golang-gitcmd\"")
//		in.WriteString("git push -u origin master")
//if _, parameters := range params{
//	in.WriteString(parameters)
//}
//	}()
//	err := cmd.Start()
//
//		log.Fatal(err)
//	}
//	log.Println(cmd.Args)
//	err = cmd.Wait()
//
//		log.Printf("Command finished with error v%\n", err)
//	}
//	fmt.Println(out.String())
//}
